<?php

namespace Tests\Unit\Models;

//use PHPUnit\Framework\TestCase;
use App\Models\Repository;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RepositoryTest extends TestCase
{
    use RefreshDatabase;
    public function test_belons_to_user()
    {
        $repository = Repository::factory()->create();
        //dd($repository->user);
        $this->assertInstanceOf(User::class,$repository->user);

    }
}
